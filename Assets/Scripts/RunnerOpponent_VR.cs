﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerOpponent_VR : MonoBehaviour
{
    private Rigidbody _rigidBody;
    public float _speedForce = 200f;
    public float _maxVelocity = 15;
    public float _randomOffsetValue = 3f;
    private Animation _runningAnimation;



    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _maxVelocity = Random.Range(_maxVelocity - _randomOffsetValue, _maxVelocity + _randomOffsetValue);
        _runningAnimation = GetComponent<Animation>();
    }


    public void SetRunnerSpeed(float velocity, float speed)
    {
        _maxVelocity = velocity;
        _speedForce = speed;
    }


    void Update()
    {
        if (GameManager._raceIsActive)
        {
            if (_rigidBody.velocity.magnitude < _maxVelocity)
            {
                _rigidBody.AddRelativeForce(0f, 0f, _speedForce * Time.deltaTime);
            }
        }
        _runningAnimation["Take 001"].speed = _rigidBody.velocity.magnitude / 5f;
    }
}
