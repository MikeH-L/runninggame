﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeScore : MonoBehaviour
{
    public static float _scoreTime;

    public Text _timeScoreTextUI;

    private float _counter;


    private void Start()
    {
        _scoreTime = 0f;
    }


    private void Update()
    {
        // If Game is Active Then Start A Counter And Calls to a method That Displays The Time on A UI Text
        if (GameManager._raceIsActive)
        {
            _counter += Time.deltaTime;
            _scoreTime = _counter;
            DisplayTimeUI();
        }
    }


    // Method That gets The Time From The Static float "_scoreTime" Variable
    public float GetTime()
    {
        float newTime = _scoreTime;
        return newTime;
    }

    private void DisplayTimeUI()
    {
        _timeScoreTextUI.text = "Time: " + Mathf.Round(_counter * 100f) / 100f;
    }
}
