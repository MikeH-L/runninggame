﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform _playerPosition;
    private Vector3 _offset;
    

    void Start()
    {
        _offset = transform.position - _playerPosition.position;
    }

    // Object with this script Follows the Player transform with the starting Offset position.

    void LateUpdate()
    {
        if (_playerPosition)
        {
        transform.position = _playerPosition.position + _offset;
        }
    }
}
