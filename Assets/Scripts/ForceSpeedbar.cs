﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ForceSpeedbar : MonoBehaviour
{
    private Slider _forceSpeedBar;
    public Rigidbody _playerRigidBody;

    private float _forceSpeedBarTresHold = 100f;


    private void Awake()
    {
        _forceSpeedBar = GetComponent<Slider>();
        _forceSpeedBar.minValue = 0f;
        _forceSpeedBar.maxValue = _forceSpeedBarTresHold;
        _forceSpeedBar.value = 0f;
    }

    private void Update()
    {
        SetForceValueOnForceBar();
    }


    // Gets The Velocity Magnitude from the players rigidbody
    // and returns it to the ForceSpeedBar

    private void SetForceValueOnForceBar()
    {
        if (_playerRigidBody)
        {
        _forceSpeedBar.value = _playerRigidBody.velocity.magnitude * 3;
        }
    }
}
