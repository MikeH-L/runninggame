﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTwo : MonoBehaviour
{
    public Rigidbody _playerRidigbodyPlayerTwo;
    public float _speedForceMovePlayerTwo = 3f;
    private bool _theOtherFootPlayerTwo;
    public Animation _runningAnimationPlayerTwo;
    public Transform _goalPositionPlayerTwo;



    void Update()
    {

        if (GameManager._raceIsActive)
        {
            if (!_theOtherFootPlayerTwo && Input.GetKeyDown(KeyCode.D) && !Input.GetKeyDown(KeyCode.A))
            {
                _theOtherFootPlayerTwo = true;
                _playerRidigbodyPlayerTwo.AddForce(Vector3.forward * _speedForceMovePlayerTwo * Time.fixedDeltaTime, ForceMode.Impulse);

            }

            if (_theOtherFootPlayerTwo && Input.GetKeyDown(KeyCode.A) && !Input.GetKeyDown(KeyCode.D))
            {
                _theOtherFootPlayerTwo = false;
                _playerRidigbodyPlayerTwo.AddForce(Vector3.forward * _speedForceMovePlayerTwo * Time.fixedDeltaTime, ForceMode.Impulse);
            }
        }
        _runningAnimationPlayerTwo["Take 001"].speed = _playerRidigbodyPlayerTwo.velocity.magnitude / 10f;
    }
}


