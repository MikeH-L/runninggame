﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    public GameManager _gameManager;
    public GameObject _cameraAngle;

    private Animation _animationCountDown;
    private Text _countDownText;

    public AudioSource _countDownAudioSource;
    public AudioClip[] _allVoiceFX;
    public AudioSource _fireStartSoundFX;

    private int _currentVoiceIndex= 0;


    private void Start()
    {
        _animationCountDown = GetComponent<Animation>();
        _countDownText = GetComponent<Text>();
        _countDownAudioSource = GetComponent<AudioSource>();
        StartCoroutine(PlayCountDownAnimationCorroutine());
    }



    // Plays an Animation Corroutine That displays strings
    // on a UI That count downs from Three to 1 And then "Run"

   private IEnumerator PlayCountDownAnimationCorroutine()
    {
        float delayTime = 1.3f;

        _countDownText.text = "3";
        _animationCountDown.Play();
        PlayVoiceSoundEffect();
         yield return new WaitForSeconds(delayTime);
        _animationCountDown.Stop();
        _countDownText.text = "2";
        _animationCountDown.Play();
        PlayVoiceSoundEffect();
        yield return new WaitForSeconds(delayTime);
        _animationCountDown.Stop();
        _countDownText.text = "1";
        _animationCountDown.Play();
        PlayVoiceSoundEffect();
        yield return new WaitForSeconds(delayTime);
        _animationCountDown.Stop();
        _countDownText.text = "RUN";
        PlayVoiceSoundEffect();
        _animationCountDown.Play();
        _cameraAngle.GetComponent<FollowPlayer>().enabled = true;
        _gameManager.RaceIsActive();
        _fireStartSoundFX.Play();
        yield return new WaitForSeconds(delayTime);
        yield return 0f;
    }




    // Plays CountDown Voice SoundEffect

    private void PlayVoiceSoundEffect()
    {
        if (_countDownAudioSource)
        {
            _countDownAudioSource.clip = _allVoiceFX[_currentVoiceIndex];
            _countDownAudioSource.Play();
            _currentVoiceIndex++;

            if (_currentVoiceIndex == _allVoiceFX.Length)
            {
                _currentVoiceIndex = 0;
            }
        }
    }
}
