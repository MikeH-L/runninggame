﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal_TwoPlayer : MonoBehaviour
{
    public GameManager _gameManager;
    private Menu _menu;

    public Text _textHighScoreNameUI;
    public Text _textHighScoreTimeUI;

    public GameObject _parentHighScoreUI;
    public GameObject[] _buttonsRestartAndBackToMenu;
    public GameObject[] _hideUiTexts;

    public AudioSource _crowdCheerGoal;

    private int _currentPositionInRace = 0;
    private int _RunnerFinsihedRace;



    private void Start()
    {
        foreach (GameObject go in _buttonsRestartAndBackToMenu)
        {
            go.SetActive(false);
        }
        _menu = FindObjectOfType<Menu>();
    }




    private void OnTriggerEnter(Collider other)
    {
        _RunnerFinsihedRace++;

        // If the Player 1 enters the goal
        if (other.gameObject.GetComponent<Player>())
        {
            string name = other.gameObject.name;
            PlayWinningSound();
            other.gameObject.GetComponent<Player>()._speedForceMove = 0f;  // Reduce The Speed of Player to Zero
            UpdateHighScoreList(name);
        }

        // If the Player 2 enters the goal
        if (other.gameObject.GetComponent<PlayerTwo>())
        {
            string name = other.gameObject.name;
            PlayWinningSound();
            other.gameObject.GetComponent<PlayerTwo>()._speedForceMovePlayerTwo = 0f; // Reduce The Speed of Player to Zero
            UpdateHighScoreList(name);
        }

        // If Both Players has Reach The Goal Then Display The HighScore UI

        if (_RunnerFinsihedRace == 2)
        {
            _parentHighScoreUI.SetActive(true);
            _gameManager.RaceIsNOTActive();
            HideGameObjects();
            foreach (GameObject go in _buttonsRestartAndBackToMenu)
            {
                go.SetActive(true);
            }
        }
    }

    // Method Thaes an argument "Players Name" And then Updates The Highscore List UI

    private void UpdateHighScoreList(string playerName)
    {
        float newTime = TimeScore._scoreTime;
        _currentPositionInRace++;
        newTime = Mathf.Round(newTime * 100f) / 100f;
        _textHighScoreNameUI.text += _currentPositionInRace + "# " + playerName + "\n";
        _textHighScoreTimeUI.text += "Time: " + newTime + "\n";
    }


    private void HideGameObjects()
    {
        foreach (GameObject item in _hideUiTexts)
        {
            item.SetActive(false);
        }
    }


    private void PlayWinningSound()
    {
        if (!_crowdCheerGoal.isPlaying)
        {
            _crowdCheerGoal.Play();
        }
    }
}
