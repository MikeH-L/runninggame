﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class VRMovement : MonoBehaviour
{
    public Rigidbody _playerRidigbody;

    public float _speedForceMove = 3f;
    private float _MovementPositionYAxis;

    private bool _legIsUp;
    private bool _OtherLegFinshed;
    public bool _useArms;

    public float _yPositionUsingArms = 11.8f;
    public float _yPositionUsingLegs = 10.6f;

    public AudioSource _crowdCheeringLong;

    private Vector3 _leftHandStartPosition;
    private Vector3 _rightHandStartPosition;

    public Transform _goalPosition;
    public Transform _leftHandTransform;
    public Transform _rightHandTransform;

    private void Start()
    {
        _leftHandStartPosition = _leftHandTransform.position;
        _rightHandStartPosition = _rightHandTransform.position;
        Debug.Log("LeftHand StartPosition" + _leftHandStartPosition);
        Debug.Log("RighttHand StartPosition" + _rightHandStartPosition);
        MovementControllerPosition();
    }



    void LateUpdate()
    {

        if (GameManager._raceIsActive)
        {
            if (_OtherLegFinshed && !_legIsUp && _leftHandTransform.position.y > _MovementPositionYAxis)
            {
                Debug.Log("Right Hand/Fot Position is over " + _MovementPositionYAxis + " in the Y axis");
                _legIsUp = true;
                _playerRidigbody.AddForce(Vector3.forward * _speedForceMove * Time.fixedDeltaTime, ForceMode.Impulse);
            }

            if (_OtherLegFinshed && _legIsUp && _leftHandTransform.position.y < _MovementPositionYAxis)
            {
                Debug.Log("Left Hand/Fot Position is over " + _MovementPositionYAxis + " in the Y axis");
                _legIsUp = false;
                _OtherLegFinshed = false;
            }

            if (!_OtherLegFinshed && !_legIsUp && _rightHandTransform.position.y > _MovementPositionYAxis)
            {
                Debug.Log("Right Hand/Fot Position is over " + _MovementPositionYAxis + " in the Y axis");
                _legIsUp = true;
                _playerRidigbody.AddForce(Vector3.forward * _speedForceMove * Time.fixedDeltaTime, ForceMode.Impulse);
            }

            if (!_OtherLegFinshed && _legIsUp && _rightHandTransform.position.y < _MovementPositionYAxis)
            {
                Debug.Log("Left  Hand/Fot Position is over " + _MovementPositionYAxis + " in the Y axis");
                _legIsUp = false;
                _OtherLegFinshed = true;
            }
        }
    }



    private float MovementControllerPosition()
    {
        if (_useArms)
        {
            _MovementPositionYAxis = _yPositionUsingArms;
            return _MovementPositionYAxis;
        }
        if (!_useArms)
        {
            _MovementPositionYAxis = _yPositionUsingLegs;
            return _MovementPositionYAxis;
        }
        return _MovementPositionYAxis;
    }
}
