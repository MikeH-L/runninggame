﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAnimationRunnerSlower : MonoBehaviour
{
    private Animation _runningAnimation;

    private void Start()
    {
        _runningAnimation = GetComponent<Animation>();
        _runningAnimation["Take 001"].speed = 1f / 6f; // Makes The Animation Model Run in Slowmotion
    }
}
