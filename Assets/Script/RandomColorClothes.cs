﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorClothes : MonoBehaviour
{

    public GameObject[] _upperBodyMaterial;
    public GameObject[] _lowerBodyMaterial;

    void Start()
    {
        Color _shirtColor = Random.ColorHSV();
        Color _pantsColor = Random.ColorHSV();


        foreach (GameObject go in _upperBodyMaterial)
        {
            go.GetComponent<Renderer>().material.color = _shirtColor;
        }

        foreach (GameObject go in _lowerBodyMaterial)
        {
            go.GetComponent<Renderer>().material.color = _pantsColor;
        }
    }
}
